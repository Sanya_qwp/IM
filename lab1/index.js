/*
  Система передачи данных работает в режиме, называемом нормальным, до появления сбоев в трех сообщениях подряд.
  В этом случае система переходит в режим аварии, в котором остается до тех пор, пока очередное сообщение не будет принято правильно.
  После этого система возвращается в нормальный режим. 
  Вероятность сбоя в очередном сообщении равна 0.1, вероятность безошибочного приема – 0.9.
  За состояния системы принять: S0 – нет сбоев; S1 – сбой в одном сообщении; S2 – сбои в двух сообщениях подряд и т.д.
  Оценить вероятность нахождения системы в следующих состояниях: нормальном, аварийном, S0, S1, S2, S3.
  Время работы системы – 10 часов; время передачи информации распределено равномерно на интервале от 3 до 7 минут; количество реализаций – 100.
*/

function getRandomFloat(min, max){
  return (Math.random() * (max - min) + min).toFixed(1);
}

function getRandomInt(min, max){ //Возвращает рандомное число, находящийся между аргументами min и max включительно
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
}

//Возвращает false при плохой детали. True при хорошей детали
function generateDetail(){
  var minFloatToFunc = 0,
      maxFloatToFunc = 1,
      failedInfoTransfer = 0.1,
      flagForInfoTransfer = getRandomFloat(minFloatToFunc, maxFloatToFunc);

  if (flagForInfoTransfer <= failedInfoTransfer) {
    return false;
  }
  else {
    return true;
  }
}

function start(){
  var s = 0, //Нормальное состояние. s == 1 - сбой в 1 сообщении, s == 2 - сбой в 2 сообщениях и т.д.
      timeOfReceipt = 0, //Время приема информации
      numOfAttempts = 100, //Количество попыток
      stateSaver = [0, 0, 0, 0]; //[s0, s1, s2, s3]
      minTimeForInfoTransfer = 3, //Минимальное время для передачи информации
      maxTimeForInfoTransfer = 7, //Максиимальное время для передачи информации
      workTimeInMinutes = 10 * 60; //Время работы системы -- 10 часов

  for (var i = 0; i < numOfAttempts; i++){
    timeOfReceipt = getRandomInt(minTimeForInfoTransfer, maxTimeForInfoTransfer);
    workTimeInMinutes -= timeOfReceipt;
    if (generateDetail() == false) {
      s++; //Переход s0->s1, s1->s2, s2->s3. Индекс s заключен в числе переменной s
      if (s < 4) {
        stateSaver[s]++;
      }
    }
    else {
      s = 0;
      stateSaver[0]++;
    }
  }
  workResults(stateSaver, numOfAttempts);
}

function workResults(stateSaver, numOfAttempts){
  var chanceToS0 = stateSaver[0] / numOfAttempts,
      chanceToS1 = stateSaver[1] / numOfAttempts,
      chanceToS2 = stateSaver[2] / numOfAttempts,
      chanceToS3 = stateSaver[3] / numOfAttempts;
  console.log("Шанс получить определенное состояние за " + numOfAttempts + " попыток.");
  console.log("Шанс на состояние s0: " + chanceToS0 + "%");
  console.log("Шанс на состояние s1: " + chanceToS1 + "%");
  console.log("Шанс на состояние s2: " + chanceToS2 + "%");
  console.log("Шанс на состояние s3: " + chanceToS3 + "%");
}